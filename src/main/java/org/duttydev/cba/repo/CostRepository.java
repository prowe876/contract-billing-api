package org.duttydev.cba.repo;

import org.duttydev.cba.domain.Cost;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CostRepository extends PagingAndSortingRepository<Cost,Long> {

}
