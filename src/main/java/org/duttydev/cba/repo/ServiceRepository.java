package org.duttydev.cba.repo;

import org.duttydev.cba.domain.Service;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends PagingAndSortingRepository<Service,String> {

}
