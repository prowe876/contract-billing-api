package org.duttydev.cba.repo;

import org.duttydev.cba.domain.ServiceOrderContract;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceOrderContractRepository extends PagingAndSortingRepository<ServiceOrderContract,Long> {

}
