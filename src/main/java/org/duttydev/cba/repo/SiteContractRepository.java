package org.duttydev.cba.repo;

import org.duttydev.cba.domain.SiteContract;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteContractRepository extends PagingAndSortingRepository<SiteContract,Long> {

}
