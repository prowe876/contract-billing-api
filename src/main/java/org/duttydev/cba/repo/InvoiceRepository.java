package org.duttydev.cba.repo;

import org.duttydev.cba.domain.Invoice;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends PagingAndSortingRepository<Invoice,Long> {

}
