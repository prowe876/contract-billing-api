package org.duttydev.cba.repo;

import org.duttydev.cba.domain.Site;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteRepository extends PagingAndSortingRepository<Site,String> {

}
