package org.duttydev.cba.web;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.duttydev.cba.domain.ServiceOrderContract;
import org.duttydev.cba.repo.ServiceOrderContractRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Path("/svc_order_contracts")
@Api(value="/svc_order_contracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ServiceOrderContractResource {
	
	@Inject
	private ServiceOrderContractRepository repo;
	
	@GET
	@ApiOperation(response=ServiceOrderContract.class, value = "Gets paginated list of ServiceOrderContracts")
	public Response getAll(@QueryParam("page") Integer page,@QueryParam("size") Integer size) {
		page = page == null ? 0 : page;
		size = size == null ? 20 : size;
		return Response.ok(repo.findAll(new PageRequest(0, 20))).build();
	}
	
	@GET
	@Path("/{id}")
	@ApiOperation(response=ServiceOrderContract.class, value = "Gets a single ServiceOrderContract")
	public Response get(@PathParam(value="id")Long id) {
		return Response.ok(repo.findOne(id)).build();
	}
	
	@POST
	@ApiOperation(response=ServiceOrderContract.class, value = "Creates a ServiceOrderContract")
	public Response create(@RequestBody ServiceOrderContract aServiceOrderContract) {
		return Response.ok(repo.save(aServiceOrderContract)).build();
	}
	
	@PUT
	@Path("/{id}")
	@ApiOperation(response=ServiceOrderContract.class, value = "Updates a ServiceOrderContract")
	public Response update(@RequestBody ServiceOrderContract aServiceOrderContract) {
		return Response.ok(repo.save(aServiceOrderContract)).build();
	}
	
	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Deletes a ServiceOrderContract")
	public Response delete(@PathParam(value="id")Long id) {
		repo.delete(id);
		return Response.ok().build();
	}
	
}
