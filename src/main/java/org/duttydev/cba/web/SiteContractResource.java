package org.duttydev.cba.web;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.duttydev.cba.domain.SiteContract;
import org.duttydev.cba.repo.SiteContractRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Path("/site_contracts")
@Api(value="/site_contracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SiteContractResource {
	
	@Inject
	private SiteContractRepository repo;
	
	@GET
	@ApiOperation(response=SiteContract.class, value = "Gets paginated list of sitecontracts")
	public Response getAll(@QueryParam("page") Integer page,@QueryParam("size") Integer size) {
		page = page == null ? 0 : page;
		size = size == null ? 20 : size;
		return Response.ok(repo.findAll(new PageRequest(0, 20))).build();
	}
	
	@GET
	@Path("/{id}")
	@ApiOperation(response=SiteContract.class, value = "Gets a single sitecontract")
	public Response get(@PathParam(value="id")Long id) {
		return Response.ok(repo.findOne(id)).build();
	}
	
	@POST
	@ApiOperation(response=SiteContract.class, value = "Creates a sitecontract")
	public Response create(@RequestBody SiteContract aSiteContract) {
		return Response.ok(repo.save(aSiteContract)).build();
	}
	
	@PUT
	@Path("/{id}")
	@ApiOperation(response=SiteContract.class, value = "Updates a sitecontract")
	public Response update(@RequestBody SiteContract aSiteContract) {
		return Response.ok(repo.save(aSiteContract)).build();
	}
	
	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Deletes a sitecontract")
	public Response delete(@PathParam(value="id")Long id) {
		repo.delete(id);
		return Response.ok().build();
	}
	
}
