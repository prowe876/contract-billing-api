package org.duttydev.cba.web;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.duttydev.cba.domain.Service;
import org.duttydev.cba.repo.ServiceRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Path("/services")
@Api(value="/services")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ServiceResource {
	
	@Inject
	private ServiceRepository repo;
	
	@GET
	@ApiOperation(response=Service.class, value = "Gets paginated list of services")
	public Response getAll(@QueryParam("page") Integer page,@QueryParam("size") Integer size) {
		page = page == null ? 0 : page;
		size = size == null ? 20 : size;
		return Response.ok(repo.findAll(new PageRequest(0, 20))).build();
	}
	
	@GET
	@Path("/{name}")
	@ApiOperation(response=Service.class, value = "Gets a single service")
	public Response get(@PathParam(value="name")String name) {
		return Response.ok(repo.findOne(name)).build();
	}
	
	@POST
	@ApiOperation(response=Service.class, value = "Creates a service")
	public Response create(@RequestBody Service aService) {
		return Response.ok(repo.save(aService)).build();
	}
	
	@PUT
	@Path("/{name}")
	@ApiOperation(response=Service.class, value = "Updates a service")
	public Response update(@RequestBody Service aService) {
		return Response.ok(repo.save(aService)).build();
	}
	
	@DELETE
	@Path("/{name}")
	@ApiOperation(value = "Deletes a service")
	public Response delete(@PathParam(value="name")String name) {
		repo.delete(name);
		return Response.ok().build();
	}
	
}
