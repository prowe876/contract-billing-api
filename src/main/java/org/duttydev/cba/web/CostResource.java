package org.duttydev.cba.web;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.duttydev.cba.domain.Cost;
import org.duttydev.cba.repo.CostRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Path("/costs")
@Api(value="/costs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CostResource {
	
	@Inject
	private CostRepository repo;
	
	@GET
	@ApiOperation(response=Cost.class, value = "Gets paginated list of costs")
	public Response getAll(@QueryParam("page") Integer page,@QueryParam("size") Integer size) {
		page = page == null ? 0 : page;
		size = size == null ? 20 : size;
		return Response.ok(repo.findAll(new PageRequest(0, 20))).build();
	}
	
	@GET
	@Path("/{id}")
	@ApiOperation(response=Cost.class, value = "Gets a single cost")
	public Response get(@PathParam(value="id")Long id) {
		return Response.ok(repo.findOne(id)).build();
	}
	
	@POST
	@ApiOperation(response=Cost.class, value = "Creates a cost")
	public Response create(@RequestBody Cost aCost) {
		return Response.ok(repo.save(aCost)).build();
	}
	
	@PUT
	@Path("/{id}")
	@ApiOperation(response=Cost.class, value = "Updates a cost")
	public Response update(@RequestBody Cost aCost) {
		return Response.ok(repo.save(aCost)).build();
	}
	
	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Deletes a cost")
	public Response delete(@PathParam(value="id")Long id) {
		repo.delete(id);
		return Response.ok().build();
	}
	
}
