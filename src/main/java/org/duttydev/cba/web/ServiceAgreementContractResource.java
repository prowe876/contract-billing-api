package org.duttydev.cba.web;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.duttydev.cba.domain.ServiceAgreementContract;
import org.duttydev.cba.repo.ServiceAgreementContractRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Path("/svc_agreement_contracts")
@Api(value="/svc_agreements_contracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ServiceAgreementContractResource {
	
	@Inject
	private ServiceAgreementContractRepository repo;
	
	@GET
	@ApiOperation(response=ServiceAgreementContract.class, value = "Gets paginated list of ServiceAgreementContracts")
	public Response getAll(@QueryParam("page") Integer page,@QueryParam("size") Integer size) {
		page = page == null ? 0 : page;
		size = size == null ? 20 : size;
		return Response.ok(repo.findAll(new PageRequest(0, 20))).build();
	}
	
	@GET
	@Path("/{id}")
	@ApiOperation(response=ServiceAgreementContract.class, value = "Gets a single ServiceAgreementContract")
	public Response get(@PathParam(value="id")Long id) {
		return Response.ok(repo.findOne(id)).build();
	}
	
	@POST
	@ApiOperation(response=ServiceAgreementContract.class, value = "Creates a ServiceAgreementContract")
	public Response create(@RequestBody ServiceAgreementContract aServiceAgreementContract) {
		return Response.ok(repo.save(aServiceAgreementContract)).build();
	}
	
	@PUT
	@Path("/{id}")
	@ApiOperation(response=ServiceAgreementContract.class, value = "Updates a ServiceAgreementContract")
	public Response update(@RequestBody ServiceAgreementContract aServiceAgreementContract) {
		return Response.ok(repo.save(aServiceAgreementContract)).build();
	}
	
	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Deletes a ServiceAgreementContract")
	public Response delete(@PathParam(value="id")Long id) {
		repo.delete(id);
		return Response.ok().build();
	}
	
}
