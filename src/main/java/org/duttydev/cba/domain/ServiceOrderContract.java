package org.duttydev.cba.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
@JsonDeserialize(as=ServiceOrderContract.class)
public class ServiceOrderContract extends AbstractContract {
	
	public ServiceOrderContract() {
		setContractType(ContractType.SERVICE_ORDER);
	}
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="contract")
	private Set<Cost> costs = new HashSet<>(0);

	public Set<Cost> getCosts() {
		return costs;
	}

	public void setSites(Set<Cost> costs) {
		this.costs = costs;
	}
	
	@JsonIgnore
	@Override
	public ContractType getContractType() {
		return super.getContractType();
	}
	
	
}
