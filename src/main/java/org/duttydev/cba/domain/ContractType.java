package org.duttydev.cba.domain;

public enum ContractType {
	SITE_CONTRACT,
	SERVICE_ORDER,
	SERVICE_AGREEMENT
}
