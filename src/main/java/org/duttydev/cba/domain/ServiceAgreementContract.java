package org.duttydev.cba.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
@JsonDeserialize(as=ServiceAgreementContract.class)
public class ServiceAgreementContract extends AbstractContract {
	
	public ServiceAgreementContract() {
		setContractType(ContractType.SERVICE_AGREEMENT);
	}
	
	@Column
	private String summary;
	
	@Column
	private String scope;
	
	@JsonIgnore
	@Override
	public ContractType getContractType() {
		return super.getContractType();
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
	
	
}
