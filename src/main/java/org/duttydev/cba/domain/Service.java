package org.duttydev.cba.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.NaturalId;

@Entity(name="CBA_SERVICE")
public class Service {
	
	@Id
	@NaturalId
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
