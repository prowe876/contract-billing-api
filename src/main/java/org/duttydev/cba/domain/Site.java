package org.duttydev.cba.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity(name="CBA_SITE")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="name")
public class Site {

	@Id
	@NaturalId
	private String name;

	@Column
	private double price;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="service_id")
	private Service service;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="site_contract_id")
	private SiteContract contract;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public SiteContract getContract() {
		return contract;
	}

	public void setContract(SiteContract contract) {
		this.contract = contract;
	}



}
