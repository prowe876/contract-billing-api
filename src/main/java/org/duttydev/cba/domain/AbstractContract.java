package org.duttydev.cba.domain;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import org.duttydev.cba.util.AbstractContractJsonDeserializer;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
@JsonDeserialize(using=AbstractContractJsonDeserializer.class)
public abstract class AbstractContract {

	@Id
	private Long id;
	
	@Column
	@ApiModelProperty(required = true,example = "2016-01-01")
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate startDate;
	
	@Column
	@ApiModelProperty(required = true,example = "2016-01-01")
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate endDate;
	
	@Column
	private Long duration;
	
	@Enumerated
	@Column
	private ChronoUnit durationUnit;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER,mappedBy="contract")
	private Set<Invoice> invoices = new HashSet<>(0);
	
	private transient ContractType contractType;

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public ChronoUnit getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(ChronoUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public Set<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ContractType getContractType() {
		return contractType;
	}

	public void setContractType(ContractType contractType) {
		this.contractType = contractType;
	}
}
