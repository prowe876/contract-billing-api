package org.duttydev.cba.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
@JsonDeserialize(as=SiteContract.class)
public class SiteContract extends AbstractContract {
	
	public SiteContract() {
		setContractType(ContractType.SITE_CONTRACT);
	}
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="contract")
	private Set<Site> sites = new HashSet<>(0);

	public Set<Site> getSites() {
		return sites;
	}

	public void setSites(Set<Site> sites) {
		this.sites = sites;
	}
	
	@JsonIgnore
	@Override
	public ContractType getContractType() {
		return super.getContractType();
	}
	
	
}
