package org.duttydev.cba.config;

import javax.ws.rs.ApplicationPath;

import org.duttydev.cba.web.CostResource;
import org.duttydev.cba.web.InvoiceResource;
import org.duttydev.cba.web.ServiceAgreementContractResource;
import org.duttydev.cba.web.ServiceOrderContractResource;
import org.duttydev.cba.web.ServiceResource;
import org.duttydev.cba.web.SiteContractResource;
import org.duttydev.cba.web.SiteResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.models.Contact;
import io.swagger.models.Info;
import io.swagger.models.License;

@Configuration
@ApplicationPath("/api")
public class JerseyConfiguration extends ResourceConfig {

	public JerseyConfiguration() {
		// Register resources
		register(ServiceResource.class);
		register(SiteResource.class);
		register(InvoiceResource.class);
		register(CostResource.class);
		register(SiteContractResource.class);
		register(ServiceOrderContractResource.class);
		register(ServiceAgreementContractResource.class);

		// Register swagger
		register(io.swagger.jaxrs.listing.ApiListingResource.class);
		register(io.swagger.jaxrs.listing.SwaggerSerializers.class);
		
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("Contract & Billing Service Api");
		beanConfig.setDescription("RESTful api that manages contract and billing information");
		beanConfig.setLicense("The MIT License (MIT)");
		beanConfig.setLicenseUrl("https://opensource.org/licenses/MIT");
		beanConfig.setContact("duttydev");
		beanConfig.setVersion("0.0.1");
		beanConfig.setSchemes(new String[]{"http"});
		beanConfig.setHost("localhost:8080");
		beanConfig.setBasePath("contract-billing-api/api");
		beanConfig.setResourcePackage("org.duttydev.cba.web");
		beanConfig.setScan(true);
		
		
		
		
	}

}
