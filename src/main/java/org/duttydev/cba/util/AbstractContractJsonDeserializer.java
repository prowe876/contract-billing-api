package org.duttydev.cba.util;

import java.io.IOException;

import org.duttydev.cba.domain.AbstractContract;
import org.duttydev.cba.domain.ContractType;
import org.duttydev.cba.domain.ServiceOrderContract;
import org.duttydev.cba.domain.SiteContract;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class AbstractContractJsonDeserializer extends JsonDeserializer<AbstractContract> {

	@Override
	public AbstractContract deserialize(JsonParser arg0, DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		AbstractContract result = null;
		
		ObjectCodec oc = arg0.getCodec();
        JsonNode node = oc.readTree(arg0);
		
		switch(ContractType.valueOf(node.get("contractType").textValue())) {
		case SITE_CONTRACT:
			result = new SiteContract();
			break;
		case SERVICE_AGREEMENT:
		case SERVICE_ORDER:
			result = new ServiceOrderContract();
			break;
		}
		
		result.setId(node.get("id").longValue());
        
		return result;
	}

}
