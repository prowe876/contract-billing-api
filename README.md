# contract-billing-api

RESTful api that manages contracting and billing information. This was done for an interview.

## Tech Stack
 - [JAX-RS - Jersey](https://jersey.java.net)
 - [Jackson](http://wiki.fasterxml.com/JacksonHome) 
 - [Swagger UI](http://swagger.io/swagger-ui/)
 - [Swagger Core Jersey](https://github.com/swagger-api/swagger-core/wiki/Swagger-Core-Jersey-2.X-Project-Setup-1.5)
 - [Spring Boot](http://projects.spring.io/spring-boot/)
 - [Spring Data JPA](http://projects.spring.io/spring-data-jpa/)
 - [Hibernate](http://hibernate.org/orm/)
 - [H2](http://www.h2database.com/html/main.html)

## UML Model Design

![UML Model](https://github.com/duttydev/contract-billing-api/blob/master/uml.png)